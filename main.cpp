#include <iostream>
#include <fstream>
#include <string>

struct Product {
    std::string name;
    int price;
    std::string category;
};

int main() {
    setlocale(LC_ALL, "RU");
    const int MAX_PRODUCTS = 100;
    Product products[MAX_PRODUCTS];
    int numProducts = 0;
    std::string input;

    while (numProducts < MAX_PRODUCTS) {
        Product product;
        std::cout << "Enter the name of product (or 'exit' to complete the input): ";
        std::getline(std::cin, input);
        if (input == "exit") {
            break;
        }
        product.name = input;

        std::cout << "Enter the price of the product: ";
        std::cin >> product.price;
        std::cin.ignore(); // очистка буфера ввода

        std::cout << "Enter the product category: ";
        std::getline(std::cin, product.category);

        products[numProducts] = product;
        numProducts++;
    }

    std::ofstream file("products.txt");
    if (file.is_open()) {
        for (int i = 0; i < numProducts; i++) {
            file << i + 1 << ". " << products[i].name << ", " << products[i].price << "р, " << products[i].category << "\n";
        }
        file.close();
        std::cout << "The data has been successfully written to the file\n";
    } else {
        std::cerr << "File opening error\n";
    }

    return 0;
}